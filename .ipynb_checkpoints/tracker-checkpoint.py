
def entities_tracker(intent, entities, current_intent, current_entities, \
                     previous_intent, previous_entities, penultimate_intent, penultimate_entities):
    if entities == []:
        entities = ['PAD']
    penultimate_intent = previous_intent
    penultimate_entities = previous_entities
    print('penultimate_intent: ', penultimate_intent)
    print('penultimate_entities: ', penultimate_entities)

    previous_intent = current_intent
    previous_entities = current_entities
    print('previous_intent: ', previous_intent)
    print('previous_entities: ', previous_entities)

    current_intent = intent
    current_entities = entities
    print('current_intent: ', current_intent)
    print('current_entities: ', current_entities)

    transfer_intent = current_intent + previous_intent + penultimate_intent
    if 'PAD' in transfer_intent:
        for index, val in enumerate(transfer_intent):
            if val == 'PAD':
                transfer_intent.pop(index)

    transfer_entitites = current_entities + previous_entities + penultimate_entities
    if 'PAD' in transfer_entitites:
        for index, val in enumerate(transfer_entitites):
            if val == 'PAD':
                transfer_entitites.pop(index)

    print('transfer_intent : ', transfer_intent)
    print('transfer_entitites : ', set(transfer_entitites))
    return current_intent, current_entities, previous_intent, previous_entities, \
           penultimate_intent, penultimate_entities, list(set(transfer_entitites)), transfer_intent



def action_tracker(action, current_action, previous_action, penultimate_action):
    if action == []:
        action = ['PAD']
    penultimate_action = previous_action
    print('penultimate_action: ', penultimate_action)

    previous_action = current_action
    print('previous_action: ', previous_action)

    current_action = [action]
    print('current_action: ', current_action)

    transfer_action = current_action + previous_action + penultimate_action
    if 'PAD' in transfer_action:
        for index, val in enumerate(transfer_action):
            if val == 'PAD':
                transfer_action.pop(index)

    print('transfer_action : ', transfer_action)
    return current_action, previous_action, penultimate_action, transfer_action

def tracker(transfer_action,transfer_entitites,transfer_intent):
    tracker = [transfer_action, transfer_entitites, transfer_intent]
    if tracker[0] == []:
        tracker.pop(0)
        tracker.insert(0, ['PAD'])
        print('action: ', tracker[0])
        print('action: ', tracker[1])
        print('action: ', tracker[2])
    return tracker
