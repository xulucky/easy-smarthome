import tensorflow as tf

tf.keras.layers.GRU

tf.keras.layers.GlobalAveragePooling1D()

tf.keras.layers.LayerNormalization()

tf.keras.layers.Dense()

tf.keras.layers.Embedding

tf.keras.layers.Layer

tf.keras.losses.binary_crossentropy

tf.keras.models.Model.fit

tf.keras.models.Model.compile()

tf.keras.layers.Bidirectional

tf.clip_by_value()

tf.clip_by_norm

tf.keras.backend.clip()

tf.nn.softmax()